# 1002. Use MSA Style

Date: 2022-04-26

## Status

Supercedes [1003. Use monolithic](1003-use-monolithic-style.md)

## Context

고려한 아키텍처 스타일로 layered, micro-kernel, msa 를 검토했다.
그중에서 MSA가 가장 fancy해 보였다.

## Decision

그래서 MSA로 결정했다. 

## Consequences

각 서버 시스템간 통신은 Event로 하고, Event broker를 뭘로 할지 결정한다.
